﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;

namespace FreedomConfederationBot
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            var Configuration = builder.Build();
            string APIKey = Configuration.GetSection("APIKey").Value;
            long ConfederationChatId = Convert.ToInt64(Configuration.GetSection("ConfederationChatId").Value);
            BotTimer timer = new BotTimer(APIKey, ConfederationChatId);
            while (!timer.HaltBot)
            {
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
