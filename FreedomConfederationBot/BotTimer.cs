﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FreedomConfederationBot
{
    class BotTimer : System.Timers.Timer
    {
        BotCore bot;
        private string APIKey;
        private long CONFEDERATION_CHAT_ID;
        public bool HaltBot = false;
        public BotTimer(string APIKey,long ConfederationChatId) : base(1000)
        {
            Enabled = true;
            AutoReset = true;
            this.APIKey = APIKey;
            CONFEDERATION_CHAT_ID = ConfederationChatId;
            bot = new BotCore(APIKey, CONFEDERATION_CHAT_ID);
            Console.WriteLine(bot.GetMeAsync().Result.Username + " is up and running");
            Elapsed += BotTimer_Elapsed;
            bot.StartReceiving();
        }

        private void BotTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Stop();
            if (bot.Restart==true)
            {
                bot.StopReceiving();
                bot = null;
                bot= new BotCore(APIKey, CONFEDERATION_CHAT_ID);
            }
            Start();
        }
    }
}
