﻿using System;
using System.Collections.Generic;
using System.Text;
using Telegram.Bot;

namespace FreedomConfederationBot
{
    public class BotCore: TelegramBotClient
    {
        private long CONFEDERATION_CHAT_ID;
        public bool Restart = false;
        public BotCore(string token, long ConfederationChatId):base(token)
        {
            CONFEDERATION_CHAT_ID = ConfederationChatId;
            OnMessage += BotCore_OnMessage;
        }

        private void BotCore_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e)
        {
            if (e.Message.Type==Telegram.Bot.Types.Enums.MessageType.Text)
            {
                
            }
        }
    }
}
